\twocolumn
* Physikalische und elektrische Größen
\footnotesize
#+ATTR_LATEX: :environment tabular :align p{2.4cm}p{2.4cm}p{1.9cm}
| *Größe*                                  | *Symbol*                        | *Einheit*          |
|----------------------------------------+-------------------------------+------------------|
| Länge                                  | @@latex: $l,s,d$@@            | m                |
| Fläche                                 | @@latex: $A$@@                | m^2               |
| Masse                                  | @@latex: $m$@@                | kg               |
| Kraft                                  | @@latex: $F$@@                | N = kg \cdot m/s^2    |
| Zeit                                   | @@latex: $t$@@                | s                |
| Temperatur                             | @@latex: $\vartheta$@@                | @@latex: $^\circ$C@@ |
| lin. Temp.-koeff.                      | @@latex: $\alpha$@@                | K^{-1}              |
|----------------------------------------+-------------------------------+------------------|
| Spannung                               | @@latex: $U=\varphi_1-\varphi_2$@@          | V                |
| Potenzial (bei x)                      | @@latex: $\varphi_x$@@               | V                |
| Strom                                  | @@latex: $I=dq/dt$@@          | A                |
| Widerstand                             | @@latex: $R$@@                | \Omega = V/A          |
| spez. Widerstand                       | @@latex: $\rho$@@                | \Omega \cdot mm^2/m        |
| Leitwert                               | @@latex: $G$@@                | S = A/V          |
| Leitfähigkeit                          | @@latex: $\kappa=1/\rho$@@            | S \cdot m/mm^2        |
| Kapazität                              | @@latex: $C$@@                | F  = As/V        |
| Induktivität                           | @@latex: $L$@@                | H = Vs/A         |
| Impedanz                               | @@latex: $Z$@@                | \Omega                |
| Blindwiderstand                        | @@latex: $X_C, X_L$@@           | \Omega                |
|----------------------------------------+-------------------------------+------------------|
| Arbeit, Energie                        | @@latex: $W=\int_0^{t_0}p(t)dt, E$@@ | J = Ws = Nm      |
| Wirkungsgrad                           | @@latex: $\eta=P_{ab}/P_zu$@@        |                  |
|----------------------------------------+-------------------------------+------------------|
| Wirkleistung                           | @@latex: $P$@@                | W                |
| Scheinleistung                         | @@latex: $S$@@                | VA               |
| Blindleistung                          | @@latex: $Q$@@                | var              |
|----------------------------------------+-------------------------------+------------------|
| el. Ladung                             | @@latex: $q$@@                | C = As           |
| el. Feldstärke                         | @@latex: $E$@@                | N/C = V/m        |
| el. Flussdichte                        | @@latex: $D$@@                | As/m^2            |
| mag. Feldstärke                        | @@latex: $H$@@                | A/m              |
| mag. Flussdichte                       | @@latex: $B$@@                | T = Vs/m^2        |
| mag. Fluss                             | @@latex: $\Phi$@@                | Wb = V\cdot s        |
| Durchflutung                           | @@latex: $\Theta$@@                | A                |
| Wicklungszahl                          | @@latex: $N$@@                |                  |
| Übersetzungs-verhältnis Trans-formator | ü=@@latex:$N_1/N_2$@@           |                  |
|----------------------------------------+-------------------------------+------------------|
| Periodendauer                          | @@latex: $T$@@                | s                |
| Frequenz                               | @@latex: $f$@@                | Hz = 1/s         |
| Kreisfrequenz                          | @@latex: $\omega$@@                | rad/s oder Hz    |
| Phasenwinkel                           | @@latex: $\varphi$@@                | rad oder $^\circ$     |
|----------------------------------------+-------------------------------+------------------|
| Permittivität                          | @@latex: $\varepsilon=\varepsilon_0 \cdot \varepsilon_r$@@        | (A \cdot s) /(V \cdot m) |
| rel. Permittivität                     | @@latex: $\varepsilon_r$@@               |                  |
| Permeabilität                          | @@latex: $\mu=\mu_0 \cdot \mu_r$@@        | (V \cdot s)/(A \cdot m)  |
| rel. Permeabilität                     | @@latex: $\mu_r$@@               |                  |

* Physikalische / elektrische Konstanten
\footnotesize
Elementarladung @@latex: $q_0 = 1,602 \cdot 10^{-19}\text{ C}$@@

Permittivitätskonst. @@latex: $\varepsilon_0=8,854 \cdot 10^{-12}\text{ (A s)/(V m)}$@@

Permeabilitätskonst. @@latex: $\mu_0=1,256 \cdot 10^{-6}\text{ (V s)/(A m)}$@@

* Gleichstromkreis
\small
@@latex: $U = R \cdot I$, $P = U \cdot I$@@

@@latex: $R = \frac{1}{G}=\rho \cdot \frac{l}{A}$, $\rho=\frac{1}{\kappa}$@@

@@latex: $R(\vartheta) = R(\vartheta_{ref}) \cdot [1+\alpha_{ref} \cdot (\vartheta-\vartheta_{ref})]$@@

@@latex: $\sum I = 0$ @@ (Knotenregel)

@@latex: $\sum U = 0$ @@ (Maschenregel)

Reihenschaltung @@latex: $R=\sum_k R_k$, $L=\sum_k L_k$, $\frac{1}{C}=\sum_k \frac{1}{C_k}$@@

Parallelschaltung @@latex: $G=\sum_k G_k$, $C=\sum_k C_k$, $\frac{1}{L}=\sum_k \frac{1}{L_k}$@@

Dreieck-Stern-Umwandlung

Sternwiderstand = @@latex: $\frac{\text{Produkt der Anliegerwiderstände}}{\text{Umfangswiderstand}}@@

Stern-Dreieck-Umwandlung

Dreiecksleitwert = @@latex: $\frac{\text{Produkt der Anliegerleitwerte}}{\text{Knotenleitwert}}@@

Ersatzquellen @@latex: $I_{K,E}=U_{LL} \cdot R_i $, $U_{LL,E}=I_K \cdot G_i$@@

Leistungsanpassung @@latex: $P_{v,max}=\frac{U_LL}{2}\cdot\frac{I_K}{2}=\frac{1}{4}\cdot\frac{U_{LL}^2}{R_i}$@@

Diodengleichung @@latex:$I_D =I_S \cdot (e^{\frac{U_D}{n\cdot U_T}} -1)$@@

* Magnetischer Kreis
\small
@@latex: $ \Theta = R_m \cdot \Phi$ @@

@@latex: $R_m = \frac{1}{\mu} \cdot \frac{l}{A}$@@

* Elektrisches Feld
\small
@@latex: $\vec{F_c}=\frac{q_1 q_2}{4 \pi \varepsilon r^2} \cdot \vec{e_r}$@@ (Coulomb-Kraft), @@latex: $\vec{F_c}=\vec{E} \cdot q_1$@@

@@latex: $q = \oint_A \vec{D} \cdot \vec{dA}$ $(q=D\cdot A)$@@

@@latex: $U = \vec{E} \cdot \vec{ds}$@@

@@latex: $C =\frac{q}{U}$@@

@@latex: $C =\varepsilon \cdot \frac{A}{d}$@@ (Plattenkondensator)

@@latex: $i_C = C \cdot \frac{du_C}{dt}$@@

@@latex: $W = \frac{1}{2} \cdot C \cdot U^2$ @@

@@latex:$\tau=RC$@@

* Magnetisches Feld
\small
@@latex: $\vec{B} = \mu\cdot \vec{H}$@@

@@latex: $\Phi = \int_A \vec{B} \cdot \vec{dA}$ ($\Phi = B \cdot A$) @@

@@latex: $\Theta = \oint_s \vec{H} \cdot \vec{ds}= \sum I =N \cdot I$@@

@@latex: $L =\frac{\Psi}{I} = \frac{N \cdot \Phi}{I}$@@

@@latex: $u_L = L \cdot \frac{di_L}{dt}$@@

@@latex: $W = \frac{1}{2} \cdot L \cdot I^2$@@

@@latex: $\vec{F} = I \cdot (\vec{l} \times \vec{B})$ @@ (Lorentz-Kraft)

@@latex: $\tau=\frac{L}{R}$@@

@@latex:$P_{mech}=2\pi n M$@@

* Wechselstrom
\small
@@latex: $\omega = 2 \pi f = \frac{2 \pi}{T}$@@

@@latex: $u(t) =\hat{u} \cdot \cos (\omega t + \varphi_u)$@@ (Zeitfunktion)

@@latex: $\bar{u}=\frac{1}{T} \int_\tau^{\tau +T}u(t)dt$@@ (Mittelwert)

@@latex: $U=\sqrt{\frac{1}{T}\int_\tau^{\tau+T}u^2(t)dt}$@@ (Effektivwert)

@@latex: $p(t) = u(t) \cdot i(t)$@@ (Augenblicksleistung)

@@latex: $P = U \cdot I \cdot \cos \varphi$@@

@@latex: $Q = U \cdot I \cdot \sin \varphi$@@

@@latex: $S =U\cdot I =\sqrt{P^2+Q^2} = \vert \underline{S}\vert$@@

@@latex: $\underline{S}=\underline{U}\cdot \underline{I}^*=P +jQ$@@

@@latex: $X_C = -\frac{1}{\omega C}$, $X_L =\omega L$@@

@@latex: $\underline{Z}=R+jX$@@

@@latex: $\Im\left{\underline{Z}\right}=0$@@ (Resonanzbedingung)

Idealer Ohmscher Widerstand @@latex: $\underline{Z}=\frac{\underline{U_R}}{\underline{I_R}}=R$@@

#+begin_src latex :results raw
\begin{circuitikz}
  \draw (0,0) to[R,i>^=$i_R(t)$,v>=$u_R(t)$,o-o] (4,0);
\end{circuitikz}
#+end_src

Ideale Induktivität @@latex: $L$@@ mit @@latex: $\underline{Z}=\frac{\underline{U_L}}{\underline{I_L}}=j\omega L$@@

#+begin_src latex :results raw
\begin{circuitikz}
  \draw (0,0) to[L,i>^=$i_L(t)$,v>=$u_L(t)$,o-o] (4,0);
\end{circuitikz}
#+end_src

Idealer Kondensator @@latex: $C$@@ mit @@latex: $\underline{Z}=\frac{\underline{U_C}}{\underline{I_C}}=\frac{1}{j\omega C}$@@

#+begin_src latex :results raw
\begin{circuitikz}
  \draw (0,0) to[C,i>^=$i_C(t)$,v>=$u_C(t)$,o-o] (4,0);
\end{circuitikz}
#+end_src

Reihenschaltung

#+begin_src latex :results raw
\begin{circuitikz}
  \draw (0,0) to[R=$R$,o-] ++(2,0) to[L=$X_L$] ++(2,0) to[C=$X_C$,-o] ++(2,0);
\end{circuitikz}
#+end_src

#+begin_src latex :results raw
\begin{tikzpicture}
  \draw[->] (4,0) -- (4,2) node[label=left:$X_L$]{};
  \draw[->] (4.2,2) -- (4.2,1) node[label=right:$X_C$]{};
  \draw[->] (0,0) -- (4,1) node[label=left:$Z$]{};
  \draw[->] (0,0) -- (4,0) node[label=below:$R$]{};
  \draw[->] (2,0) arc (0:15:2) node[label=left:$\varphi$]{};
  \draw[->,OwlBlue] (0,-.2) -- (5,-.2) node[label=above:$I$]{};
\end{tikzpicture}
#+end_src

Parallelschaltung

#+begin_src latex :results raw
\begin{circuitikz}
  \draw (0,0) to[short,o-*] ++(1.5,0) node[name=r1]{} to[short,*-*] ++(1.5,0) node[name=l1]{} to[short,*-] ++(1.5,0) node[name=c1]{};
  \draw (0,-2) to[short,o-*] ++(1.5,0) node[name=r2]{} to[short,*-*] ++(1.5,0) node[name=l2]{} to[short,*-] ++(1.5,0) node[name=c2]{};
  \draw (r1) to[R=$G$] (r2);
  \draw (l1) to[L=$B_L$] (l2);
  \draw (c1) to[C=$B_C$] (c2);
\end{circuitikz}
#+end_src

#+begin_src latex :results raw
\begin{tikzpicture}
  \draw[->] (4,0) -- (4,2) node[label=left:$B_C$]{};
  \draw[->] (4.2,2) -- (4.2,1) node[label=right:$B_L$]{};
  \draw[->] (0,0) -- (4,1) node[label=left:$Y$]{};
  \draw[->] (0,0) -- (4,0) node[label=below:$G$]{};
  \draw[->] (2,0) arc (0:15:2) node[label=left:$\varphi$]{};
  \draw[->,OwlGreen] (0,-.2) -- (5,-.2) node[label=above:$U$]{};
\end{tikzpicture}
#+end_src

* Drehstrom
\small
#+NAME: fig:drehstrom
#+begin_src latex :results raw
\begin{circuitikz}
  %% Source
  \coordinate (Csource);
  \foreach \anch/\ang in {L1/0,L3/-240,L2/-120}{%
    \draw (Csource) node[label=below:N]{} to[vsourcesin=$\angle \ang^\circ$,*-*] +({\ang}:3) coordinate (\anch) node[anchor={\ang},label=right:\anch]{};
  }
  %% Load
%  \coordinate (Cload)  at ($(Csource)+(10,0)$);
%  \foreach \anch/\ang in {a/0,b/-120,c/-240}{%
%    \draw (Cload) to[R=$R_{\anch}$,*-*] +({-\ang+90}:3) coordinate (L\anch) node[anchor={\ang}]{(L\anch)};
%  }
  %% Connections
%  \draw (L1) -- +(0,1) to[R] ($(La)+(0,1)$) -- (La);
%  \draw (L2) to[R] (Lb);
%  \draw (L3) -- +(0,-2) to[R] ($(Lc)+(0,-2)$) -- (Lc);
\end{circuitikz}
#+end_src

Leiterspannung: @@latex: $U_\Delta=U=U_{L1}-U_{L2}$@@ (z. B.)

symmetrisch: @@latex: $P=3\cdot P_{\text{Str}}=\sqrt{3}\cdot U\cdot I \cdot \cos\varphi$@@

unsymmetrisch: @@latex: $P=P_{\text{Str1}} +P_{\text{Str2}} + P_{\text{Str3}}$@@

* Komplexe Zahlen
\small
@@latex: $\sqrt{-1}=j=e^{j90^\circ}$@@

@@latex: $\frac{1}{j}=\frac{1\cdot j}{j \cdot j}=e^{-j90^\circ}=-j$@@

@@latex: $\underline{z}=a +jb=r \cdot e^{j\varphi}=r \cdot(\cos \varphi +j \sin \varphi)$@@

@@latex: $\underline{z}^*=a-jb=r\cdote^{-j\varphi}=r\cdot(\cos \varphi -j\sin \varphi)$@@

@@latex: $r=\vert\underline{z}\vert=\sqrt{a^2+b^2}$@@

@@latex: $\varphi=\text{arg}\left(\underline{z}\right)=\begin{cases}\arctan\frac{b}{a}&\text{für } a > 0\\
\arctan\frac{b}{a}+\pi&\text{für }a<0,b\geq0\\
\arctan\frac{b}{a}-\pi&\text{für }a<0,b<0\\
\frac{\pi}{2}&\text{für }a=0,b>0\\
-\frac{\pi}{2}&\text{für }a=0,b<0\\
\text{n. d.}&\text{für }a=0,b=0
 \end{cases}$\\@@

** COMMENT Dreiecksbeziehungen
#+NAME: fig:dreieck
#+begin_src latex :results raw
\begin{center}
  \begin{tikzpicture}
    \coordinate[label=below:A] (O) at (0,0);
    \coordinate[label=below:B] (A) at (4,0);
    \coordinate[label=above:C] (B) at (1,2);
    \draw (O)--(A)--(B)--cycle;

    \tkzLabelSegment[below=2pt](O,A){\textit{c}}
    \tkzLabelSegment[left=2pt](O,B){\textit{b}}
    \tkzLabelSegment[above right=2pt](A,B){\textit{a}}

    \tkzMarkAngle[fill= THKoelnOrange,size=0.65cm,%
    opacity=.4](A,O,B)
    \tkzLabelAngle[pos = 0.35](A,O,B){$\alpha$}

    \tkzMarkAngle[fill= THKoelnOrange,size=0.9cm,%
    opacity=.4](B,A,O)
    \tkzLabelAngle[pos = 0.7](B,A,O){$\beta$}

    \tkzMarkAngle[fill= THKoelnOrange,size=0.7cm,%
    opacity=.4](O,B,A)
    \tkzLabelAngle[pos = 0.5](O,B,A){$\gamma$}
  \end{tikzpicture}
\end{center}
#+end_src


@@latex: $\sin^2 \alpha + \sin^2 \beta =1$@@

@@latex: $\frac{\sin\alpha}{\sin\beta}=\frac{a}{b}$@@ (Sinussatz)

@@latex: $c^2=a^2+b^2-2ab \cdot \cos \gamma$@@ (Kosinussatz)
** COMMENT Integrale
@@latex: $\int \frac{1}{x}dx=\ln x$@@



\normalsize
\onecolumn
