#+STARTUP: BEAMER
\normalsize
* Lernziele und Struktur dieser Einheit
** Lernziele dieser Vorlesung :B_frame:
*** Sie berechnen die *Temperatur eines Widerstands*
*** Sie nutzen dafür Formeln für *Metalle und Halbleiter*
*** Sie ermitteln den notwendigen *thermischen Widerstand eines Kühlkörpers*
* Vorstellung des heutigen Anwendungsbeispiels
** Heizleistung einer Heizwendel maximieren vs. Kühlkörper auslegen
   Heizwendel (soll heiß werden) aus Kanthal mit @@latex:$\rho_{20}=0,09\frac{\Omega \cdot\text{mm}^2}{\text{m}}$@@, @@latex:$\alpha_{20}=0,0175\text{/K} $@@ (Werte aus \cite{kanthalcom_resistance_2021}) und dem Querschnitt @@latex:$A=0,5\text{ mm}^2$@@ bei @@latex:$\vartheta=20^\circ \text{C}$@@ an einem Gleichstromgenerator (lineare Spannungsquelle) mit @@latex:$U_q=10\text{ V}$@@ und @@latex:$R_i =3\text{ }\Omega$@@ betreiben

   #+BEAMER:\pause

   Welche Heizdrahtlänge ermöglicht es, die maximale Leistung aus dem Generator zu holen?

   #+BEAMER:\pause

   Anders gefragt: welcher Lastwiderstand zieht die maximale Leistung?

   #+BEAMER:\pause

   Und: wie bestimmt man jetzt den Wirkungsgrad?

   #+BEAMER:\pause

   Wie ermittelt man für einen Widerstand, der *nicht* (zu) heiß werden soll den richtigen Kühlkörper? 

** Wie wir das Problem lösen werden: 

   #+BEAMER:\pause
   - Zählpfeilsysteme wiederholen
   #+BEAMER:\pause
   - Temperaturverhalten ermitteln
   #+BEAMER:\pause
   - Leistung und Energie an Quellen und Lasten ermitteln
   #+BEAMER:\pause
   - Verlustleistung und Wirkungsgrad berechnen
   #+BEAMER:\pause
   - optimale Leistung \to Leistungsanpassung durchführen
   #+BEAMER:\pause
   - Kühlkörper auslegen
     
* Wiederholung Zählpfeilsysteme und Ergänzung Temperaturverhalten

** Erzeuger und Verbraucher

@@latex: \begin{tcolorbox}\textbf{Verbraucherzählpfeilsystem}: gleichsinnige Bezugspfeile von Spannung $U$ und Strom $I$, falls $P>0$ wirkt Eintor als Verbraucher, falls $P<0$ als Erzeuger\end{tcolorbox}@@

#+BEAMER:\pause

*** Erzeuger :B_column:
    :PROPERTIES:
    :BEAMER_env: column
    :BEAMER_col: 0.5
    :END:
    
   #+NAME: fig-erzeuger
   #+begin_src latex :results raw
   \begin{figure}[h]
     \centering
    \begin{circuitikz}
      \draw (0,0) coordinate (u1) to[short,o-] ++(1,0) coordinate (b1);
      \draw (0,-2) coordinate (u2) to[short,o-] ++(1,0) coordinate (b2);
      \draw (b2) to[battery1,i_<={\color{OwlBlue}{$I=2\text{ A}$}}] (b1);
      \draw (u1) to[open,v_={\color{OwlGreen}{$U=48\text{ V}$}}] (u2);
      \draw[->,width=3,color=THKoelnRed,yshift=1em] (-1.5,-1) -- ++(1,0) node[label=above:$\color{THKoelnRed}{P(t)}$];
    \end{circuitikz}
    \caption{Leistung positiv (Batterie lädt)}\label{fig-erzeuger}
    \end{figure}
   #+end_src

#+BEAMER: \pause
*** Verbraucher :B_column:
    :PROPERTIES:
    :BEAMER_env: column
    :BEAMER_col: 0.5
    :END:

    #+NAME: fig-verbraucher
    #+begin_src latex :results raw
    \begin{figure}[h]
      \centering
    \begin{circuitikz}
     \draw (0,0) coordinate (u1) to[short,o-] ++(1,0) coordinate (b1);
     \draw (0,-2) coordinate (u2) to[short,o-] ++(1,0) coordinate (b2);
     \draw (b2) to[battery1,i_<={\color{OwlBlue}{$I=-10\text{ A}$}}] (b1);
     \draw (u1) to[open,v_={\color{OwlGreen}{$U=48\text{ V}$}}] (u2);
     \draw[<-,width=3,color=THKoelnRed,yshift=1em] (-1.5,-1) -- ++(1,0) node[label=above:$\color{THKoelnRed}{P(t)}$];
    \end{circuitikz}
    \caption{Leistung negativ (Batterie entlädt)}\label{fig-verbraucher}
    \end{figure}
#+end_src 

** Kontrollfrage zu Zählpfeilsystemen: Erzeuger oder Verbraucher?

*** Teil 1 :B_column:
    :PROPERTIES:
    :BEAMER_env: column
    :BEAMER_col: .5
    :END:
    #+NAME: fig-zpsbsp1
    #+begin_src latex :results raw
    \begin{figure}[h]
      \centering
    \begin{circuitikz}
      \draw (0,0) 
      node[twoportshape,t=?] (tw) {}
      (tw.150) to[short,-o,xshift=.1em,i_<=$\color{OwlBlue}{i(t)}$,color=OwlBlue] ++(-1,0)
      (tw.-150) to[short,-o,xshift=.1em] ++(-1,0);
      \draw (-1.7,.4) to[open,v^>=$\color{OwlGreen}{u(t)}$,color=OwlGreen] ++(0,-.8);
      \draw[<-,color=THKoelnRed,width=3] (-2.5,0) node[label=left:$\color{THKoelnRed}{p(t)}$] --++ (1,0);
    \end{circuitikz}
    \caption{$i(t)>0$,$u(t)>0$,$p(t)>0$}\label{fig-zpsbsp1}
    \end{figure}
#+end_src 
   
    #+NAME: fig-zpsbsp2
    #+begin_src latex :results raw
    \begin{figure}[h]
      \centering
    \begin{circuitikz}
      \draw (0,0) 
      node[twoportshape,t=?] (tw) {}
      (tw.30) to[short,-o,xshift=-.1em,i>=$\color{OwlBlue}{i(t)}$,color=OwlBlue] ++(1,0)
      (tw.-30) to[short,-o,xshift=-.1em] ++(1,0);
      \draw (1.7,.4) to[open,v>=$\color{OwlGreen}{u(t)}$,color=OwlGreen] ++(0,-.8);
      \draw[->,color=THKoelnRed,width=3] (2,0) --++ (1,0) node[label=above:$\color{THKoelnRed}{p(t)}$];
    \end{circuitikz}
    \caption{$i(t)>0$,$u(t)>0$,$p(t)>0$}\label{fig-zpsbsp2}
    \end{figure}
#+end_src 

    #+NAME: fig-zpsbsp3
    #+begin_src latex :results raw
    \begin{figure}[h]
      \centering
    \begin{circuitikz}
      \draw (0,0) 
      node[twoportshape,t=?] (tw) {}
      (tw.30) to[short,-o,xshift=-.1em,i<=$\color{OwlBlue}{i(t)}$,color=OwlBlue] ++(1,0)
      (tw.-30) to[short,-o,xshift=-.1em] ++(1,0);
      \draw (1.7,.4) to[open,v>=$\color{OwlGreen}{u(t)}$,color=OwlGreen] ++(0,-.8);
      \draw[<-,color=THKoelnRed,width=3] (2,0) --++ (1,0) node[label=above:$\color{THKoelnRed}{p(t)}$];
    \end{circuitikz}
    \caption{$i(t)>0$,$u(t)>0$,$p(t)>0$}\label{fig-zpsbsp3}
    \end{figure}
#+end_src 

*** Teil 2 :B_column:
    :PROPERTIES:
    :BEAMER_env: column
    :BEAMER_col: .5
    :END:

    #+NAME: fig-zpsbsp4
    #+begin_src latex :results raw
    \begin{figure}[h!]
      \centering
    \begin{circuitikz}
      \draw (0,0) 
      node[twoportshape,t=?] (tw) {}
      (tw.150) to[short,-o,xshift=.1em,i_>=$\color{OwlBlue}{i(t)}$,color=OwlBlue] ++(-1,0)
      (tw.-150) to[short,-o,xshift=.1em] ++(-1,0);
      \draw (-1.7,-.4) to[open,v_>=$\color{OwlGreen}{u(t)}$,color=OwlGreen] ++(0,.8);
      \draw[<-,color=THKoelnRed,width=3] (-2,0) --++ (-1,0) node[label=above:$\color{THKoelnRed}{p(t)}$];
    \end{circuitikz}
    \caption{$i(t)>0$,$u(t)>0$,$p(t)>0$}\label{fig-zpsbsp4}
    \end{figure}
#+end_src 

    #+NAME: fig-zpsbsp5
    #+begin_src latex :results raw
    \begin{figure}[h!]
      \centering
    \begin{circuitikz}
      \draw (0,0) 
      node[twoportshape,t=?] (tw) {}
      (tw.30) to[short,-o,xshift=-.1em,i^>=$\color{OwlBlue}{i(t)}$,color=OwlBlue] ++(1,0)
      (tw.-30) to[short,-o,xshift=-.1em] ++(1,0);
      \draw (1.7,.4) to[open,v>=$\color{OwlGreen}{u(t)}$,color=OwlGreen] ++(0,-.8);
      \draw[->,color=THKoelnRed,width=3] (2,0) --++ (1,0) node[label=above:$\color{THKoelnRed}{p(t)}$];
    \end{circuitikz}
    \caption{$i(t)<0$,$u(t)>0$,$p(t)<0$}\label{fig-zpsbsp5}
    \end{figure}
#+end_src 

    #+NAME: fig-zpsbsp6
    #+begin_src latex :results raw
    \begin{figure}[h]
      \centering
    \begin{circuitikz}
      \draw (0,0) 
      node[twoportshape,t=?] (tw) {}
      (tw.150) to[short,-o,xshift=.1em,i^<=$\color{OwlBlue}{i(t)}$,color=OwlBlue] ++(-1,0)
      (tw.-150) to[short,-o,xshift=.1em] ++(-1,0);
      \draw (-1.7,-.4) to[open,v>=$\color{OwlGreen}{u(t)}$,color=OwlGreen] ++(0,.8);
      \draw[<-,color=THKoelnRed,width=3] (-2,0) --++ (-1,0) node[label=above:$\color{THKoelnRed}{p(t)}$];
    \end{circuitikz}
    \caption{$i(t)<0$,$u(t)>0$,$p(t)<0$}\label{fig-zpsbsp6}
    \end{figure}
#+end_src 

** Zurück zum Anwendungsbeispiel

   Skizzieren Sie die Schaltung mit Strom- und Spannungspfeilen sowie dem Leistungsfluss. 

\small   Heizwendel aus Kanthal mit @@latex:$\rho_{20}=0,09\frac{\Omega \cdot\text{mm}^2}{\text{m}}$@@, @@latex:$\alpha_{20}=0,0175\text{/K} $@@ (Werte aus \cite{kanthalcom_resistance_2021}) und dem Querschnitt @@latex:$A=0,5\text{ mm}^2$@@ bei @@latex:$\vartheta=20^\circ \text{C}$@@ an einem Gleichstromgenerator (lineare Spannungsquelle) mit @@latex:$U_q=10\text{ V}$@@ und @@latex:$R_i =3\text{ }\Omega$@@ betreiben

Heizwendel erreicht im Betrieb eine Temperatur von @@latex:$\vartheta_{\text{an}} = 80^\circ\text{C}$@@

   Welche Heizdrahtlänge ermöglicht es, die maximale Leistung aus dem Generator zu holen?
\normalsize

** Widerstand der Heizwendel bei Betriebstemperatur

Ermitteln Sie den spezifischen elektrischen Widerstand @@latex:$\rho_{80} = \rho(\vartheta=80^\circ\text{C})$@@ der Heizwendel bei Betriebstemperatur.

Zur Erinnerung:
Heizwendel aus Kanthal mit @@latex:$\rho_{20}=0,09\frac{\Omega \cdot\text{mm}^2}{\text{m}}$@@, @@latex:$\alpha_{20}=0,0175\text{/K} $@@ (Werte aus \cite{kanthalcom_resistance_2021}) und dem Querschnitt @@latex:$A=0,5\text{ mm}^2$@@ bei @@latex:$\vartheta=20^\circ \text{C}$@@

#+BEAMER:\pause

@@latex:$[\rho_{80}=0.1845\frac{\Omega \cdot \text{mm}^2}{\text{m}}]$@@

** Temperaturverhalten von Metallen

#+NAME: fig-temperklaerungmetall
#+ATTR_latex: :width .6\textwidth :height \textheight :options angle=0,keepaspectratio :float nil
#+CAPTION: In Metallen werden die Elektronen in ihrer Bewegung bei steigender Temperatur durch das stärkere Wackeln der Atomrümpfe ausgebremst \to Widerstand steigt @@latex:$\alpha>0$@@ @@latex:\textcolor{gray}{Eberhard Waffenschmidt}@@
[[../ge1img/temperaturverhalten_metall.png]]

** Temperaturverhalten von Halbleitern

#+NAME: fig-temperklaerunghalbleiter
#+ATTR_latex: :width .6\textwidth :height \textheight :options angle=0,keepaspectratio :float nil
#+CAPTION: In Halbleitern können sich die Elektronen umso besser bewegen, umso wärmer es wird \to Widerstand sinkt @@latex:$\alpha<0$@@ @@latex:\textcolor{gray}{Eberhard Waffenschmidt}@@
[[../ge1img/temperaturverhalten_halbleiter.png]]


* Leistung, Energie, Volllaststunden

** Zur Erinnerung: Leistung und Energie
*** Beispiel
   Eine Elektrofahrzeugbatterie wird mit einem Ladegerät der *Leistung* @@latex:$P=3\text{ kW}$@@ geladen. Dies geschieht über einen Zeitraum von @@latex:$\Delta t=2\text{ h}$@@. Der Energiezähler an derLadesäule gibt die aufsummierte Leistung über der Zeit an. Wie viel Energie wurde geladen - in Joule, Wattstunden und Kilowattstunden?

   #+BEAMER:\pause

   @@latex:$E=\int_{t_1}^{t_2} p(t) dt=P \cdot \Delta t= 3\text{ kW} \cdot 2 \text{ h}=6\text{ kWh}$@@

   #+BEAMER:\pause

   @@latex:$E=6000\text{ Wh}$@@

   #+BEAMER:\pause

   @@latex:$E=21,6\text{ MJ}$@@

** Volllaststunden und mittlere Leistung

#+name: fig-volllaststundenermitteln
#+begin_src python :results file :session :var matplot_lib_filename=(org-babel-temp-file "figure" ".png"),fontsize=fs :exports results
import matplotlib.pyplot as plt
import numpy as np
plt.style.use('classic')

#plt.rcParams.update({'font.size':fontsize})
#rcParams.update({'figure.autolayout': True})

t=np.linspace(0,8760,80)
np.random.seed(seed=0)
p=np.random.rand(len(t),1)**3
p=p/np.max(p)
P=np.mean(p)*np.ones(len(t))
Pmax=np.max(p)*np.ones(len(t))
vollasth=(8760/80)*np.sum(p)
nutzungsgrad=np.mean(p)

plt.figure(figsize=(8,3))
plt.plot(t,p,'b-',label='$p(t)/P_{max}$')
plt.plot(t,P,'r--',label='$P_{mittel}=E/t$')
plt.plot(t,Pmax,'g--',label='$P_{max}$')
plt.xlabel('Zeit $t$ [h]')
plt.xlim(0,8760)
plt.ylabel('Leistung $p(t)/P_{pk}$')
plt.ylim(0,1.7)
plt.legend(loc='upper right')
plt.plot([vollasth,vollasth],[0,1.7],'k:')
plt.annotate(str(int(np.around(vollasth,decimals=0))),xy=(vollasth+100,1.2))
plt.annotate('Volllaststunden',xy=(vollasth+100,1.1))
plt.annotate('Nutzungsgrad',xy=(5850,nutzungsgrad+.2),color='red')
plt.annotate(str(int(np.around(100*nutzungsgrad,decimals=0)))+'%',xy=(5850,nutzungsgrad+.1),color='red')

plt.tight_layout()
plt.savefig(matplot_lib_filename)
matplot_lib_filename
#+end_src

#+CAPTION: Leistung an einer Windenergieanlage (fiktive Daten)
#+LABEL: fig-volllaststundenermitteln
#+ATTR_LATEX: :width \textwidth :height \textheight :options angle=0,keepaspectratio :float nil
#+RESULTS: fig-volllaststundenermitteln
[[file:/tmp/babel-A6NnOT/figureQrUdBn.png]]

#+BEAMER:\pause

@@latex:\begin{tcolorbox}
\textbf{Definition Volllaststunden}: Volllaststunden entsprechen der Zeit, in der die selbe Energiemenge bei konstanter Spitzenleistung erzeugt werden würde.
\end{tcolorbox}@@

** Ein paar Worte dazu :B_ignoreheading:
   :PROPERTIES:
   :BEAMER_env: ignoreheading
   :END:

   Abb. [[fig-volllaststundenermitteln]] zeigt den Zusammenhang zwischen einem zeitlich variablen Erzeugungsprofil, der mittleren Leistung @@latex:$P_{mittel}=E/t$@@, dem Nutzungsgrad @@latex:$\frac{P_{mittel}}{P_{max}}$@@ und den Volllaststunden @@latex:$t_{Volllast}$@@. Würde die fiktive Windenergieanlage dauerhaft und konstant bei mittlerer Leistung laufen, so würde sie eine Leistung von z. B. @@latex:$P_{Mittel}= 375\text{ kW}$@@ erzeugen und zwar während des gesamten Jahres, also während @@latex:$8760$@@ Stunden jedes Jahr. Dabei speist sie @@latex:$E=P_{mittel}\cdot t_{Jahr}=375\text{ kW}\cdot 8760\text{ h}=3,3\cdot 10^6 \text{ kWh}=3,3\text{ GWh}$@@ ins elektrische Stromnetz ein. Diese Energie ließe sich auch bestimmen, indem man zu jeder Stunde den in dieser Stunde produzierten Wert der Energie hinzuzählt. Da die Anlage eine Spitzenleistung (/peak power/) von @@latex:$P_{pk}=1,5\text{ MW}$@@ besitzt, wird sie also im Mittel mit einem Nutzungsgrad von @@latex:$375\text{ kW}/1,5\text{ MW}=25\%$@@ betrieben. Würde man die Anlage bei Spitzenleistung laufen lassen, bis die Jahresenergie erzeugt wäre, würde man sie also nur @@latex:$E/P_{pk}=2183$@@ *Volllaststunden* laufen lassen.

   /Hinweis: Ein Jahr hat @@latex:$365*24=8760$@@ Stunden./

* Verlustleistung, Wirkungsgrad, Wirkungsgrad-Ketten
** Verlustleistung und Wirkungsgrad

#+NAME: fig-quelleverbraucherverluste
#+begin_src latex :results raw
\begin{figure}[h]
  \centering
  \begin{circuitikz}
    \draw (0,0) to[short,o-] ++(-2,0) to[european voltage source=Quelle] ++(0,-1.5) to[short,-o] ++(2,0);
    \draw[dashed,color=gray] (-2.75,.25) --++(3,0) --++(0,-2) --++(-3,0) --++(0,2);
    \draw (1.5,0) to[short,o-*] ++(.5,0) to[short,*-*] ++(1,0) to[short,*-o] ++(.5,0);
    \draw (1.5,-1.5) to[short,o-*] ++(.5,0) to[short,*-*] ++(1,0) to[short,*-o] ++(.5,0);
    \draw[dashed,color=gray] (1,.25) --++(3,0) --++(0,-2) --++(-3,0) --++(0,2);
    \draw (4.5,0) to[short,o-] ++(2,0) to[resistor=Verbraucher] ++(0,-1.5) to[short,-o] ++(-2,0);
    \draw[dashed,color=gray] (4.25,.25) --++(4.5,0) --++(0,-2) --++(-4.5,0) --++(0,2);
    \draw[->,width=3,color=THKoelnRed,yshift=1em, thick] (0,-1.25) -- ++(1,0) node[label=above:$\color{THKoelnRed}{P(t)}$];
  \end{circuitikz}
  \caption{Eine Quelle (Quellenleistung $P_Q$) erzeugt Nutzleistung $P_N$ und Verlustleistung $P_V$}\label{fig-quelleverbraucherverluste}
\end{figure}
#+end_src

** Leistungsflüsse

#+name: fig-einfachessankey
#+begin_src python :results file :session :var matplot_lib_filename=(org-babel-temp-file "figure" ".png"),fontsize=fs :exports results
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt
import matplotlib.dates as mdates
import numpy as np
from matplotlib.sankey import Sankey

sankey = Sankey(unit=None)
sankey.add(flows=[1, -0.8, -0.2],
           orientations=[0, 0, -1],
           labels=['$P_Q$', '$P_N$', '$P_V$'],
           rotation=0,
           trunklength=1.0)
sankey.finish()

#plt.rcParams.update({'font.size':fontsize})
#rcParams.update({'figure.autolayout': True})
plt.tight_layout()
plt.savefig(matplot_lib_filename)
matplot_lib_filename
#+end_src

#+CAPTION: Quellenleistung teilt sich auf in Nutzleistung und Verlustleistung (sog. Sankey-Diagramm)
#+LABEL: fig-einfachessankey
#+ATTR_LATEX: :width .6\textwidth :height \textheight :options angle=0,keepaspectratio :float nil
#+RESULTS: fig-einfachessankey
[[file:/tmp/babel-A6NnOT/figureOp19Ib.png]]


#+BEAMER:\pause

** Definition Wirkungsgrad (=Effizienz)

\begin{equation}
\text{Wirkungsgrad}=\frac{\text{Nutzen}}{\text{Aufwand}}=\frac{P_N}{P_Q} = \frac{P_N}{P_N+P_V} = \frac{P_Q - P_V}{P_Q} = 1-\frac{P_V}{P_Q}
\end{equation}

** Leistung an linearen Quellen

#+NAME: fig-quellemitverbraucher
#+begin_src latex :results raw
\begin{figure}[h]
  \centering
  \begin{circuitikz}
    \draw (0,0) to[european voltage source=$U_q$] ++(0,-1.5);
    \draw (0,0) to[resistor=$R_i$,-o] ++(4,0);
    \draw (0,-1.5) to[short,-o] ++(4,0);
    \draw (4,0) to[open,v>=$U$] ++(0,-1.5);
    \draw (4,0) to[short,i>=$I$] ++(2,0) to[thermistor=$R_{Last}$] ++(0,-1.5) to[short,-o] ++(-2,0);
    \draw[->,width=3,color=THKoelnRed,yshift=1em] (-1.5,-1) -- ++(1,0) node[label=above:$\color{THKoelnRed}{P(t)}$];
  \end{circuitikz}
  \caption{Variabler Verbraucher (Last) an Quelle}\label{fig-quellemitverbraucher}
\end{figure}
#+end_src
   
#+BEAMER:\pause

#+name: fig-leistungskennlinienlinquelle
#+begin_src python :results file :session :var matplot_lib_filename=(org-babel-temp-file "figure" ".png"),fontsize=fs :exports results
import matplotlib.pyplot as plt
import numpy as np
plt.style.use('classic')

#plt.rcParams.update({'font.size':fontsize})
#rcParams.update({'figure.autolayout': True})

u=np.linspace(0,1,1000)
i=1-u
p=u*i
p1=p[250]*np.ones(len(u))

plt.figure(figsize=(8,3))
plt.plot(u,u,'g-',label='$U/U_q$')
plt.plot(u,i,'b-',label='$I/I_K$')
plt.plot(u,p,'r-',label='$P/P_q$')
plt.plot(u,p1,'orange',label='$P_1/P_q$')
plt.grid()
plt.xlabel('Spannung $U/U_q$')
plt.xlim(0,1)
plt.ylim(0,1)
plt.legend(loc='upper center')
plt.annotate('zwei Kombinationen von $I$ und $U$',xytext=(0.3,0.1),xy=(u[250],p[250]),arrowprops=dict(facecolor='orange',shrink=0.05))
plt.annotate('zwei Kombinationen von $I$ und $U$',xytext=(0.3,0.1),xy=(u[750],p[750]),arrowprops=dict(facecolor='orange',shrink=0.05))
plt.annotate('Kurzschluss $U=0$',xytext=(0.1,0.5),xy=([0,0]),arrowprops=dict(facecolor='black',shrink=0.05))
plt.annotate('Leerlauf $U =\infty$',xytext=(0.7,0.5),xy=([1,0]),arrowprops=dict(facecolor='black',shrink=0.05))
plt.legend(bbox_to_anchor=(1,1), loc="upper left")

plt.tight_layout()
plt.savefig(matplot_lib_filename)
matplot_lib_filename
#+end_src

#+CAPTION: Ausgangskennlinien einer linearen Quelle
#+LABEL: fig-leistungskennlinienlinquelle
#+ATTR_LATEX: :width \textwidth :height \textheight :options angle=0,keepaspectratio :float nil
#+RESULTS: fig-leistungskennlinienlinquelle
[[file:/tmp/babel-mrB6Ug/figureErgKWU.png]]

** Leistungsanpassung an linearen Quellen

   #+NAME: fig-quellemitverbraucherangepasst
   #+begin_src latex :results raw
   \begin{figure}[h]
     \centering
     \begin{circuitikz}
       \draw (0,0) to[european voltage source=$U_q$] ++(0,-1.5);
       \draw (0,0) to[resistor=$R_i$,-o] ++(4,0);
       \draw (0,-1.5) to[short,-o] ++(4,0);
       \draw (4,0) to[open,v>=${U=\frac{U_q}{2}}$] ++(0,-1.5);
       \draw (4,0) to[short,i>=$I$] ++(2,0) to[resistor=${R_{Last}=R_i}$] ++(0,-1.5) to[short,-o] ++(-2,0);
       \draw[->,width=3,color=THKoelnRed,yshift=1em] (-1.5,-1) -- ++(1,0) node[label=above:$\color{THKoelnRed}{P=\frac{1}{4}\cdot\frac{U_q^2}{R_i}}$,xshift=2em];
     \end{circuitikz}
     \caption{Maximale Leistung abrufen: $R_{Last}=R_i$}\label{fig-quellemitverbraucherangepasst}
    \end{figure}
   #+end_src
   
   #+BEAMER:\pause

#+name: fig-leistungskennlinienlinquelleangepasst
#+begin_src python :results file :session :var matplot_lib_filename=(org-babel-temp-file "figure" ".png"),fontsize=fs :exports results
import matplotlib.pyplot as plt
import numpy as np
plt.style.use('classic')

#plt.rcParams.update({'font.size':fontsize})
#rcParams.update({'figure.autolayout': True})

u=np.linspace(0,1,1000)
i=1-u
p=u*i
p1=p[250]*np.ones(len(u))

plt.figure(figsize=(8,3))
plt.plot(u,u,'g-',label='$U/U_q$')
plt.plot(u,i,'b-',label='$I/I_K$')
plt.plot(u,p,'r-',label='$P/P_q$')
plt.plot(u,p1,'orange',label='$P_1/P_q$')
plt.grid()
plt.xlabel('Spannung $U/U_q$')
plt.xlim(0,1)
plt.ylim(0,1)
plt.legend(loc='upper center')
plt.annotate('Leistungsanpassung',xytext=(0.3,0.1),xy=(u[500],p[500]),arrowprops=dict(facecolor='orange',shrink=0.05))
plt.annotate('Kurzschluss $U=0$',xytext=(0.1,0.5),xy=([0,0]),arrowprops=dict(facecolor='black',shrink=0.05))
plt.annotate('Leerlauf $U =\infty$',xytext=(0.7,0.5),xy=([1,0]),arrowprops=dict(facecolor='black',shrink=0.05))
plt.legend(bbox_to_anchor=(1,1), loc="upper left")

plt.tight_layout()
plt.savefig(matplot_lib_filename)
matplot_lib_filename
#+end_src

#+CAPTION: Ausgangskennlinien einer linearen Quelle
#+LABEL: fig-leistungskennlinienlinquelleangepasst
#+ATTR_LATEX: :width \textwidth :height \textheight :options angle=0,keepaspectratio :float nil
#+RESULTS: fig-leistungskennlinienlinquelleangepasst
[[file:/tmp/babel-mrB6Ug/figuremTpzsc.png]]

** Leistungsanpassung an linearen Quellen in Worten        :B_ignoreheading:
:PROPERTIES:
:BEAMER_env: ignoreheading
:END:

Die Darstellung [[fig-leistungskennlinienlinquelleangepasst]] zeigt, dass es ein Maximum gibt bei @@latex:$U/U_q=0,5$@@. Man kann dieses Maximum auch analytisch berechnen:

Die Leistung @@latex:$P$@@ am Lastwiderstand @@latex:$R_L$@@ berechnet sich aus:

\begin{equation}
P = U\cdot I
\end{equation}

Mit dem Ohmschen Gesetz erhält man:

\begin{equation}
I = \frac{U}{R_L}
\end{equation}

Das ergibt für die Leistung:

\begin{equation}
P = \frac{U^2}{R_L}
\end{equation}

Die Spannung hängt noch vom Widerstand @@latex:$R_L$@@ ab. Mit dem Spannungsteiler erhalten wir:

\begin{equation}
\frac{U}{U_q} = \frac{R_L}{R_L + R_i} \rightarrow U = U_q \cdot \frac{R_L}{R_L+R_i}
\end{equation}

Nun erhalten wir für die Leistung:

\begin{equation}
P = \frac{U_q^2 \cdot R_L^2 }{R_L \cdot (R_L + R_i)^2} = \frac{U_q^2 \cdot R_L}{(R_L + R_i)^2}
\end{equation}

Das Maximum der Leistung erhält man, indem man die Gleichung für @@latex:$P(R_L)$@@ nach @@latex:$R_L$@@ ableitet und dann die Nullstellen der Ableitung ermittelt:

\begin{equation}
\frac{\text{d}P}{\text{d}R_L} = \frac{\text{d}}{\text{d}R_L} \left[\frac{U_q^2 \cdot R_L}{(R_L + R_i)^2}\right]
\end{equation}

\begin{equation}
\rightarrow \frac{\text{d}P}{\text{d}R_L} = U_q^2 \cdot \left[\frac{1\cdot (R_L + R_i)^2 - (2R_L +2 R_i)\cdot R_L}{(R_L + R_i)^4}\right]
\end{equation}

\begin{equation}
\rightarrow \frac{\text{d}P}{\text{d}R_L} = U_q^2 \cdot \frac{R_L^2 + 2R_L R_i + R_i^2 - 2R_L^2 - 2R_i R_L}{(R_L + R_i)^4}
\end{equation}

\begin{equation}
\rightarrow \frac{\text{d}P}{\text{d}R_L} = U_q^2 \cdot \frac{-R_L^2 + R_i^2}{(R_L + R_i)^4}
\end{equation}

Nun ermitteln wir die Nullstellen und berücksichtigen dabei, dass @@latex:$U_q>0$@@ und @@latex:$R_L + R_i>0$@@ sein müssen, damit diese Betrachtung überhaupt interessant ist:

\begin{equation}
0 = U_q^2 \cdot \frac{-R_L^2 + R_i^2}{(R_L + R_i)^4} \vert \cdot \frac{1}{U_q^2}
\end{equation}

\begin{equation}
\rightarrow 0 = \frac{R_i^2 - R_L^2}{(R_L + R_i)^4} \vert \cdot (R_L + R_i)^4
\end{equation}

\begin{equation}
\rightarrow 0 = R_i^2 - R_L^2 \rightarrow R_L^2 = R_i^2 \rightarrow R_L = R_i
\end{equation}

@@latex:\begin{tcolorbox}
Das bedeutet, dass die Leistung, die die lineare Quelle abgibt, maximal wird, wenn R_L = R_i$.
\end{tcolorbox}@@

** Zurück zum Anwendungsbeispiel

\small Heizwendel aus Kanthal mit @@latex:$\rho_{20}=0,09\frac{\Omega \cdot\text{mm}^2}{\text{m}}$@@, @@latex:$\alpha_{20}_{}=0,0175\text{/K}$@@ und dem Querschnitt @@latex:$A=0,5\text{ mm}^2$@@ bei @@latex:$\vartheta=20^\circ \text{C}$@@ an einem Gleichstromgenerator (lineare Spannungsquelle) mit @@latex:$U_q=10\text{ V}$@@ und @@latex:$R_i =3\text{ }\Omega$@@ betreiben

Welche Heizdrahtlänge ermöglicht es, die maximale Leistung aus dem Generator zu holen? Annahme: Betriebstemperatur @@latex:$\vartheta=80^\circ\text{C}$@@ mit @@latex:$\rho_{80}=0,1845\frac{\Omega \cdot \text{mm}^2}{\text{ m}}$@@
\normalsize

#+beamer:\pause

@@latex:$P_{max}$ für $R_{heiz}=R_i$, @@
#+beamer:\pause

@@latex:$R_{heiz}=\rho_{80} \cdot \frac{l}{A}$, @@
#+BEAMER:\pause

@@latex:$l=\frac{R_i A}{\rho_{80}}=8,13\text{ m}$@@

** Wenn ein Widerstand nicht heiß werden soll: Kühlkörper auslegen

Im Anwendungsbeispiel sollte der Widerstand = Heizwendel heiß werden

/thermischer Übergangswiderstand/ \neq elektrischer Widerstand

@@latex:$R_{th}$@@ beschreibt, wie warm ein Bauteil wird, wenn es Leistung abgibt

#+NAME: fig-thermuebergangswiderstand
#+ATTR_latex: :width .5\textwidth :height \textheight :options angle=0,keepaspectratio :float nil
#+CAPTION: Thermischer Übergangswiderstand @@latex:\textcolor{gray}{Eberhard Waffenschmidt}@@
[[../ge1img/therm_uebergangswiderstand.png]]

** Thermischer Übergangswiderstand aus einem Datenblatt

#+NAME: fig-thermuebergangswiderstanddb
#+ATTR_latex: :width .5\textwidth :height \textheight :options angle=0,keepaspectratio :float nil
#+CAPTION: Thermischer Übergangswiderstand aus einem Datenblatt @@latex:\textcolor{gray}{Eberhard Waffenschmidt}@@
[[../ge1img/therm_uebergangswiderstand_bauteil.png]]

** Thermischer Übergangswiderstand einer Leiterplatte

#+NAME: fig-thermuebergangswiderstandpcb
#+ATTR_latex: :width .5\textwidth :height \textheight :options angle=0,keepaspectratio :float nil
#+CAPTION: Thermischer Übergangswiderstand einer Leiterplatte @@latex:\textcolor{gray}{Eberhard Waffenschmidt}@@
[[../ge1img/therm_uebergangswiderstand_leiterplatte.png]]

* Zusammenfassung                                                   :B_frame:
  :PROPERTIES:
  :BEAMER_env: frame
  :END:
*** Begriffe und Formeln :B_column:
:PROPERTIES:
    :BEAMER_env: column
    :BEAMER_col: .7
    :END:
- Wirkungsgrad: Nutzleistung durch aufgewendete Leistung
#+BEAMER:\pause
- in Serie geschaltete Wirkungsgrad multiplizieren
#+BEAMER:\pause
- Volllaststunden: falls maximale Leistung dauernd gefahren wird äquivalenter Energieverbrauch in Zeit x erreicht
#+BEAMER:\pause
- Energie: Leistung mal Zeit
#+BEAMER:\pause
- Leistungsanpassung: @@latex:$R_i=R_L$@@
#+BEAMER:\pause
- thermische Effekte
*** Hausaufgaben :B_column:
:PROPERTIES:
    :BEAMER_env: column
    :BEAMER_col: .3
    :END:
- Übungsaufgaben, Tutorium
#+BEAMER:\pause
- Praktikum vorbereiten und nachbereiten
\normalsize
* Videos Eberhard Waffenschmidt
** Folge 04: Temperaturabhaengigkeit Widerstand
- Prinzip für
  - Metalle
  - Halbleiter
- Beschreibung als Gleichung
- Materialien
Video: https://youtu.be/pWjbprEiGjE
Präsentation als PDF-Dokument: [[http://www.waffenschmidt-aachen.homepage.t-online.de/lehre/GLE/GLE_04-Widerstand_Temperaturabhaengigkeit-SoSe2020.pdf][GLE_04-Widerstand_Temperaturabhaengigkeit-SoSe2020.pdf]]

** Folge 40: Thermischer Übergangswiderstand
- Bauteile
- Kühlkörper
- Flächen
Video: https://youtu.be/RWtQATN9pbs
Präsentation als PDF-Dokument: [[http://www.waffenschmidt-aachen.homepage.t-online.de/lehre/GLE/GLE_40-Kuehlung.pdf][GLE_40-Kuehlung.pdf]]

* Übungsaufgaben
** Verlustleistung an einem Motor
    Auf dem Typenschild eines Motors findet sich die Angabe @@latex:$\eta=81\%$@@. Die mechanische Leistung, die der Motor an die Welle abgibt, beträgt @@latex:$2\text{ kW}$@@. Bestimmen Sie die mechanische Leistung, die der Motor aufnimmt (d.h. die Nutzleistung @@latex:$P_N$@@) sowie die elektrischen Verluste im Motor @@latex:$P_V$@@. 

    @@latex:$[P_N=1,62\text{ kW}, P_V=380\text{ W}]$@@

** Warmwasserboiler
    In einem Badezimmer befindet sich ein Warmwasserboiler mit Anschlusswert @@latex:$1,3\text{ kW}$@@. Sein Wirkungsgrad beträgt @@latex:$69\%$@@. Wie lange dauert es, bis @@latex:$5\text{ l}$@@ Wasser von @@latex:$10^\circ \text{C}$@@ auf @@latex:$55^\circ \text{C}$@@ erwärmt sind? Dafür nötige Energie: @@latex:$W_{aufheiz}=cm\Delta\vartheta=4,2\text{ kJ/kg K}\cdot 5\text{ kg}\cdot 45\text{ K}=945\text{ kJ}$@@

    @@latex:$[P_{aufheiz}=897\text{ W}$, $\Delta t= 1054\text{ s}\approx18\text{ min.}\approx0,3\text{ h}]$@@

** Heizkartusche

    In einer Maschine ist eine sog. Heizkartusche verbaut. Diese besteht aus einer Metallwendel mit einem definierten Widerstand, der mit Strom durchflossen wird sowie aus einem Vergussmaterial, das gut Wärme leitet. So kann die Heizkartusche lokal Wärme aufbringen in der Maschine. Die Heizkartusche wird üblicherweise mit @@latex:$U_1=24\text{ V}$@@ betrieben. Nun erhöht sich die Spannung in der Spannungsversorgung auf @@latex:$U_2=25\text{ V}$@@. Wieviel höher ist die Heizleistung bei @@latex:$U_2$@@ als bei @@latex:$U_1$@@?

    @@latex:$[8,5\%]$@@

** Elektrokran

    Ein Elektromotor treibt einen Hebekran. Dieser soll eine Masse von @@latex:$m=850\text{ kg}$@@ mit einer Geschwindigkeit von @@latex:$0,3\text{ m/s}$@@ gegen die Erdbeschleunigung @@latex:$g=9,81\text{ m/s}^2$@@ anheben. Der Gesamtwirkungsgrad des Krans beträgt @@latex:$\eta=0,45$@@. Welche Leistung @@latex:$P$@@ muss der Motor elektrisch abgeben?

    @@latex:$[P_{mech}=2,5\text{ kW}$, $P_{el}=5,6\text{ kW}]$@@
** Leiterquerschnitt
    Eine Camping-Leuchte nimmt bei einer anliegenden Spannung von @@latex:$U=12\text{ V}$@@ eine Leistung von @@latex:$200\text{ W}$@@ auf. Sie soll über eine zweiadrige Leitung der Länge @@latex:$8\text{ m}$@@ am Nachbar-Camping-Platz angeschlossen werden. Die Leitungsadern sind aus Kupfer mit dem spezifischen Widerstand von @@latex:$17,6\cdot10^{-9}\text{ }\Omega\text{m}$@@. Bestimmen Sie den Mindestquerschnitt @@latex:$A$@@ jeder Leitungsader (Hin- und Rückleitung!), damit die in der Leitung auftretende Verlustleistung einen Wert von @@latex:$2\%$@@ nicht übersteigt. Welchen Durchmesser müsste diese Leitung mindestens haben?

    @@latex:$[P_V=I^2 \cdot R_L$, $I=\frac{P}{U}$, $R_L=2\rho\frac{l}{A}$, $P_V=(P/U)^2 \cdot 2\rho \frac{l}{A}$, $A=\frac{P^2 2\rho l}{U^2 P_V}=39,11\text{ mm}^2$, $D\approx7,1\text{ mm} $@@

** Leistungsanpassung

    Ein Gleichstromgenerator hat einen Innenwiderstand von @@latex:$R_i=2,7\text{ }\Omega$@@ und eine Quellenspannung von @@latex:$U_q=10\text{ V}$@@. Wenn der Leitungswiderstand von Hin- und Rückleitung zum Verbraucher nun @@latex:$R_L=1\text{ }\Omega$@@ beträgt, welchen Widerstand @@latex:$R_V$@@ muss der Verbraucher haben, damit die Quelle die maximale Leistung abgeben kann? Skizzieren Sie dafür ein Diagramm, in dem die Leistung am Verbraucher über dem Widerstand aufgetragen ist. 

    @@latex:$R_V=R_i-R_L=1,7\text{ }\Omega$@@
** Volllaststunden
    Eine Photovoltaik-Anlage erzeugt im Jahr @@latex:$E=3500\text{ kWh}$@@. Die Volllaststunden betragen @@latex:$t_V=1200\text{ h/a}$@@. Wie groß ist die Spitzenleistung @@latex:$P_{pk}$@@ der Photovoltaikanlage? Wie groß ist der Nutzungsgrad @@latex:$N$@@?

** Wirkungsgradketten
    Gegeben ist ein System wie in Abb. [[fig-blockschaltbildsystemuebung]] dargestellt. 

   #+NAME: fig-blockschaltbildsystemuebung
   #+ATTR_latex: :width .7\textwidth :height \textheight :options angle=0,keepaspectratio :float nil
   #+CAPTION: Blockschaltbild eines Energiesystems für die Aufgabe @@latex:\textcolor{gray}{Eberhard Waffenschmidt}@@
   [[../ge1img/blockbildwirkungsgradeuebung.png]]

   - Berechnen Sie @@latex:$P_{V1}$@@, @@latex:$P_{V2}$@@, @@latex:$P_{G1}$@@, @@latex:$P_{G2}$@@ und @@latex:$P_{G}$@@.
   - Wie groß ist der Gesamtwirkungsgrad des Systems?

** Leistungsanpassung einer linearen Spannungsquelle

    Gegeben ist eine lineare Spannungsquelle mit @@latex:$R_i=3\text{ }\Omega$@@ und @@latex:$U_q=12\text{ V}$@@. Am Ausgang ist ein Potenziometer, d.h. ein variabler Widerstand angeschlossen. 
    - Skizzieren Sie die Schaltung.
    - Welche Quelle @@latex:$P_{max}$@@ kann die Quelle liefern?
    - Wie groß muss dann der Lastwiderstand @@latex:$R_L$@@ sein?
    - Nehmen Sie an, dass nun @@latex:$R_L=6\text{ }\Omega$@@ beträgt. Wie groß ist dann die Leistung am Lastwiderstand?

** Nutzungsgrad

    Eine Kleinwasserkraftanlage mit Nennleistung @@latex:$P_N=50\text{ kW}$@@ erzeugt im Jahr @@latex:$E=50\text{ MWh}$@@ Energie. Berechnen Sie die mittlere Leistung @@latex:$P_m$@@, den Nutzungsgrad @@latex:$N$@@ und die Volllaststunden @@latex:$t_V$@@. 

** Wirkungsgrad einer linearen Spannungsquelle

    Gegeben ist eine lineare Spannungsquelle mit Innenwiderstand @@latex:$R_i=30\text{ }\Omega$@@ und Quellenspannung @@latex:$12\text{ V}$@@. 
    - Skizzieren Sie die Schaltung der Quelle zusammen mit einem angeschlossenen Lastwiderstand @@latex:$R_L$@@.
    - Berechnen Sie den Kurzschlussstrom @@latex:$I_K$@@ der Quelle.
    - Wie groß muss der Widerstand @@latex:$R_L$@@ sein, damit der der Kurzschlussstrom fließt?
    - Wie groß ist die Verlustleistung @@latex:$P_V$@@ im Innenwiderstand @@latex:$R_i$@@ der Quelle, wenn der Kurzschlussstrom fließt?
    - Wie groß ist der Wirkungsgrad @@latex:$\eta$@@?

** Leistungswerte eines Systems

    Gegeben ist ein System und die Zeitverläufe von Strom und Spannung. 

    #+NAME: fig-systemmitpfeilen
    #+begin_src latex :results raw
    \begin{figure}[h]
      \centering
    \begin{circuitikz}
      \draw (0,0) 
      node[twoportshape,t=?] (tw) {}
      (tw.150) to[short,-o,xshift=.1em,i<=$\color{OwlBlue}{i(t)}$] ++(-1,0)
      (tw.-150) to[short,-o,xshift=.1em] ++(-1,0);
      \draw (-1.7,.4) to[open,v^>=$\color{OwlGreen}{u(t)}$] ++(0,-.8);
      \draw[<-,color=THKoelnRed,width=3] (-3.5,0) --++ (1,0) node[label=above:$\color{THKoelnRed}{p(t)}$];
    \end{circuitikz}
    \caption{System für die Übungsaufgabe}\label{fig-systemmitpfeilen}
    \end{figure}
#+end_src 
 
   #+name: fig-leistungswerteermittelnaufgabe
#+begin_src python :results file :session :var matplot_lib_filename=(org-babel-temp-file "figure" ".png"),fontsize=fs :exports results
import matplotlib.pyplot as plt
import numpy as np
plt.style.use('classic')

plt.rcParams.update({'font.size':fontsize})
rcParams.update({'figure.autolayout': True})

t=np.linspace(0,24,10000)
i=np.piecewise(t,[t<4,(t>=4)&(t<6),(t>=6)&(t<8),(t>=8)&(t<10),(t>=10)&(t<18),(t>=18)&(t<22),t>=22],[0,3,7,10,-5,0,3])
u=np.piecewise(t,[t<4,(t>=4)&(t<10),(t>=10)&(t<14),(t>=14)&(t<18),(t>=18)&(t<22),t>=22],[0,10,-2,-5,0,2])
p=i*u

plt.figure(figsize=(12,9))

ax1=plt.subplot(311)
plt.plot(t,i,'b-')
plt.ylabel('Strom $i(t)$ [A]')
plt.ylim(-6,12)
plt.grid()
plt.setp(ax1.get_xticklabels(),visible=False)

ax2=plt.subplot(312,sharex=ax1)
plt.plot(t,u,'g-')
plt.ylabel('Spannung $u(t)$ [V]')
plt.ylim(-6,12)
plt.grid()
plt.setp(ax2.get_xticklabels(),visible=False)

ax3=plt.subplot(313,sharex=ax1)
plt.plot(t,p,'w-')
plt.ylabel('Leistung $p(t)$ [W]')
plt.xlim(0,24)
#plt.ylim(-6,12)
plt.grid()

plt.xlabel('Zeit $t$ [h]')

plt.tight_layout()
plt.savefig(matplot_lib_filename)
matplot_lib_filename
#+end_src

#+CAPTION: Zeitverläufe von Strom und Spannung (oben und Mitte), und zugehöriger Zeitverlauf der Leistung (selbst eintragen)
#+LABEL: fig-leistungswerteermittelnaufgabe
#+ATTR_LATEX: :width \textwidth :height \textheight :options angle=0,keepaspectratio :float nil
#+RESULTS: fig-leistungswerteermittelnaufgabe
[[file:/tmp/babel-11768JJv/figure11768Mvk.png]]

- Zeichnen Sie die momentane Leistung @@latex:$p(t)$@@ in das Diagramm ein.
- Wie groß ist die mittlere Leistung @@latex:$P_m$@@?
- Wie groß ist die über einen Tag (24 h) verbrauchte Energie @@latex:$E$@@?
- Wie groß sind die Volllaststunden der Batterie?

** Mit Wirkungsgradketten rechnen

    In einem Energiesystem sind mehrere Komponenten in Serie miteinander verschaltet. Die Leistung der Quelle @@latex:$P_G$@@ wandelt ein erstes Übertragungssystem mit dem Wirkungsgrad @@latex:$\eta_1$@@ in die Verlustleistung @@latex:$P_{V1}$@@ und die Nutzleistung @@latex:$P_1$@@. Das zweite Übertragungssystem ist in Serie an dieses angeschlossen. Es wandelt die Leistung @@latex:$P_1$@@ in die Verlustleistung @@latex:$P_{V2}$@@ und die Nutzleistung @@latex:$P_2$@@. Ebenso ist ein drittes System an das zweite angeschlossen, welches die Leistung @@latex:$P_2$@@ in die Verlustleistung @@latex:$P_{V3}$@@ und die Verbraucherleistung @@latex:$P_L$@@ mit dem Wirkungsgrad @@latex:$\eta_3$@@ umwandelt. Tragen Sie die fehlenden Werte in der Tabelle ein.

| Variante | @@latex:$P_G$@@           | @@latex:$\eta_1$@@   | @@latex:$\eta_2$@@   | @@latex:$\eta_3$@@   | @@latex:$\eta_{ges}$@@ | @@latex:$P_V$@@ |
| a)       | @@latex:$50\text{ W}$@@  | @@latex:$90\%$@@ | @@latex:$80\%$@@ | @@latex:$95\%$@@ |                  |                |
| b)       | @@latex:$200\text{ W}$@@ | @@latex:$95\%$@@ | @@latex:$99\%$@@ |                  |                  | @@latex:$150\text{ W}$@@ |

