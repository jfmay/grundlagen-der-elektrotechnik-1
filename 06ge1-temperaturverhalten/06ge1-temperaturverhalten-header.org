# -*- coding: utf-8 -*-
# -*- mode: org -*-

#+STARTUP: BEAMER

#+TITLE:    Grundlagen der Elektrotechnik 1 (GE1)
#+SUBTITLE: 6 Temperaturverhalten
#+AUTHOR:   \href{mailto:johanna.may@th-koeln.de}{Prof. Dr. Johanna Friederike May}
#+INSTITUTION: Institut für Elektrische Energietechnik (IET) und Cologne Institute for Renewable Energy (CIRE)
#+EMAIL: johanna.may@th-koeln.de
#+DATE:     \today
#+DESCRIPTION:  
#+KEYWORDS: 
#+LANGUAGE: de

#+OPTIONS:  H:2 texht:t toc:nil title:nil

# #+OPTIONS:  TeX:t LaTeX:t skip:nil d:nil todo:t pri:nil tags:not-in-toc
# #+INFOJS_OPT: view:nil toc:t ltoc:t mouse:underline buttons:0  path:https://orgmode.org/org-info.js
#+EXPORT_SELECT_TAGS: export
#+EXPORT_EXCLUDE_TAGS: noexport
#+LINK_UP:
#+LINK_HOME:

#+INCLUDE: "../slidehead.org"
# #+INCLUDE: "../GE1skript/printhead2.org"

# # Glossar bisher funktioniert das nach der Anleitung [[https://orgmode.org/worg/exporters/beamer/beamer-dual-format.html][_hier_]] nicht
# #+INCLUDE: "../GE1skript/glossary.org"

# #+name: set-slide-flag
# #+begin_src emacs-lisp :exports results :results value latex
# (setq hjh-exporting-slides 't)
# ""
# #+end_src

# #+call: makegloss :exports (if hjh-exporting-slides "results" "none") :results value latex
# #+results: makegloss

\begin{frame}
\maketitle
\begin{tikzpicture}[overlay, remember picture]
\node[above right=7.5cm and 9.3cm of current page.south west] {\includegraphics[width=2cm]{../ge1img/thk.pdf}};
\end{tikzpicture}
\end{frame}

\frame[t]{\tableofcontents}

#+include: "./06ge1-temperaturverhalten.org" :minlevel 1

* Verwendete Literatur
** Verwendete Literatur
  :PROPERTIES:
  :BEAMER_opt: fragile, allowframebreaks
  :END:
#+begin_export latex
\topskip=2cm\advance\textheight by -2cm\enlargethispage{-1cm}
\printbibliography[heading=none]
#+end_export
